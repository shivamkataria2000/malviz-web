const mongoose = require("mongoose");

const sessionSchema = new mongoose.Schema({
	sessionid: String,
	name: String,
	userid: String
});

const activeSession= mongoose.model('Session', sessionSchema);
module.exports = activeSession;