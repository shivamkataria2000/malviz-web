const express =require('express');
const userController = require(`./../controllers/userController`);
const router =express.Router();


router
	.route('/')
	.get(userController.getAllUsers)
	.post(userController.createUser);

router
	.route('/login')
	.patch(userController.updateUser)
	.delete(userController.deleteUser);
router
	.route('/welcome')
	.get(userController.welcome);

module.exports=router;