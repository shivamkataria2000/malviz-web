const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const uuid = require("uuid/v4");
const session = require("express-session");
const graphRouter = require("./routes/graphRoutes");
const userRouter = require("./routes/userRoutes");
const mainRouter = require("./routes/mainRoutes");
const activeSession = require("./models/sessionModel");
const User = require("./models/userModel");
const cookieParser = require('cookie-parser');

//1 middleware

passport.use(
	new LocalStrategy(
		{
			usernameField: "email"
		},
		(email, password, done) => {
			User.findOne({ email: email }, (err, user) => {
				if (err) {
					return done(err);
				}
				if (!user) {
					return done(null, false, {
						message: "Incorrect username."
					});
				}
				let flag=1;
				user.comparePassword(password, (err, match) => {
					if (!match) {
						flag=0;
					}
				});
				if(!flag){
					return done(null, false, {
							message: "Incorrect password."
						});
				}
				return done(null, user);
			});
		}
	)
);

passport.serializeUser((user, done) => {
	console.log(
		"Inside serializeUser callback. User id is save to the session file store here"
	);
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	console.log("Inside deserializeUser callback");
	console.log(
		`The user id passport saved in the session file store is: ${id}`
	);
	User.findById(id, function(err, user) {
		done(err, user);
	});
});

const app = express();

if (process.env.NODE_ENV === "development") {
	app.use(morgan("dev"));
}
//app.use(cookieParser);

app.use(
	session({
		genid: req => {
			return uuid();
		},
		secret: "topsecret",
		resave: false,
		saveUninitialized: true
	})
);
app.use(express.json());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(`${__dirname}/templates/templated-transit`));

app.use((req, res, next) => {
	req.requestTime = new Date().toISOString();
	console.log(req.user);
	next();
});

//routes

app.use("/api/v1/graphs", graphRouter);
app.use("/api/v1/users", userRouter);
app.use("/",mainRouter);

app.all("*", (req, res, next) => {
	res.status(404).json({
		status: "fail",
		message: `cant find ${req.orignalUrl} on this server!!`
	});
});

module.exports = app;
