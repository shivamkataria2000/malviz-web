const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema({
	name: String,
	email: {
		type: String,
		match: [
			/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
			"Please fill a valid email address"
		],
		required: 'Email address is required',
		unique: true,
		lowercase: true,
		trim: true
	},
	password: {
		type: String,
		required: 'Password is required'
	},
	uploads:[String]
});

userSchema.pre("save", function(next) {

    this.password = bcrypt.hashSync(this.password, 10);
    console.log(this.password);
    next();
});

userSchema.methods.comparePassword = function(plaintext, callback) {
    return callback(null, bcrypt.compareSync(plaintext, this.password));
};

const User= mongoose.model('User', userSchema);
module.exports = User;