const express =require('express');
const passport=require('passport');
const userController = require(`./../controllers/userController`);
const graphController = require(`./../controllers/graphController`);
const router =express.Router();


router
	.route('/')
	.get(userController.getAllUsers)
	.post(userController.createUser);

router
	.route('/login')
	.get(userController.login)
	.post(passport.authenticate("local", {
		successRedirect: "/welcome",
		failureRedirect: "/log-in.html"
	}))
	.patch(userController.updateUser)
	.delete(userController.deleteUser);
router
	.route('/welcome')
	.get(userController.welcome);
router
	.route('/logout')
	.get(userController.logout);
router
	.route('/upload')
	.post(userController.uploadFile);
router
	.route('/signup')
	.get(userController.signup)
	.post(userController.createUser);
router
	.route('/mygraphs')
	.get(graphController.getMyGraphs);
router
	.route('/mygraph')
	.get(graphController.getGraph);
router
	.route('/graph')
	.get(graphController.renderGraphTemplate);
	
module.exports=router;

//	.get(graphController.getMyGraphs);