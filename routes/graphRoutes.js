const express=require('express');
const graphController = require('./../controllers/graphController');


const router =express.Router();

//router.param('id',graphController.checkID);
//router.route('/top-5-malware').get(graphController.aliasTopGraphs , graphController.getAllGraphs);
router.route('/graph-stats').get(graphController.getGraphStats);

router
	.route('/')
	.get(graphController.getAllGraphs)
	.post(graphController.createGraph);
router
	.route('/:id')
	.patch(graphController.updateGraph)
	.delete(graphController.deleteGraph)
	.get(graphController.getGraph);

module.exports = router;