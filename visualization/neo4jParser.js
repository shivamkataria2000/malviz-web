const fs = require("fs");
const util = require("util");

function parseGraph(file = `${__dirname}/allnew.json`) {
    console.log(file);
    var neo4jJson=fs.readFileSync(file, "utf-8");
    var rows = neo4jJson.split("\n");
    var neo4jObj = rows.map(JSON.parse);
    //console.log(neo4jObj);
    var nodes = [];
    var edjes = [];
    var colors = [
        "#9c2121",
        "#ffba87",
        "#71a83a",
        "#beedcd",
        "#ff8a7",
        "#e0eb1c",
        "#b723a5",
        "#f3c238",
        "#59b6eb",
        "#2b2a6f",
        "#f05c15",
    ];
    //map clusters/labels to cluster ids cid
    var clusters = new Map();
    // map cid to number of nodes of that cluster
    var idcount = new Map();
    // set of node ids
    var ids = new Set();
    //count is number of clusters
    var count = 0;
    neo4jObj.forEach(function(row) {
        // node
        if (row.type === "node") {
            let title =
                '<h3 style="color:white;">' +
                util.inspect(row.properties).slice(1, -1) +
                "</h3>";
            title = title.replace(/,/g, "<br>");
            // check if Label is seen for first time
            if (!clusters.has(row.labels[0])) {
                // fix cluster id to map
                count++;
                clusters.set(row.labels[0], count);
                // itialize number of nodes of label x as 1
                idcount[row.labels[0]] = 1;
                //push node properties to node array
                if (row.labels[0] == "HOSTS_IP") {
                    let country_code = row.properties.iso_code;
                    nodes.push({
                        id: row.id,
                        label: row.labels[0],
                        cid: clusters.get(row.labels[0]),
                        shape: "image",
                        image: `https://www.countryflags.io/${country_code}/shiny/64.png`,
                        brokenImage: "../images/broken.png",
                        title: title,
                    });
                } else {
                    nodes.push({
                        id: row.id,
                        shape: "circle",
                        label: row.labels[0],
                        cid: clusters.get(row.labels[0]),
                        color: colors[clusters.get(row.labels[0]) % 11],
                        value: 25,
                        title: title,
                    });
                }
                ids.add(row.id);
            }
            // Limit one lable nodes to 10
            else if (idcount[row.labels[0]] < 10) {
                if (row.labels[0] == "HOSTS_IP") {
                    let country_code = row.properties.iso_code;
                    console.log(row.properties);
                    console.log(
                        `https://www.countryflags.io/${country_code}/shiny/64.png`
                    );
                    nodes.push({
                        id: row.id,
                        label: row.labels[0],
                        cid: clusters.get(row.labels[0]),
                        shape: "image",
                        image: `https://www.countryflags.io/${country_code}/shiny/64.png`,
                        brokenImage: "../images/broken.png",
                        title: title,
                    });
                } else {
                    nodes.push({
                        id: row.id,
                        shape: "circle",
                        label: row.labels[0],
                        cid: clusters.get(row.labels[0]),
                        color: colors[clusters.get(row.labels[0]) % 11],
                        value: 25,
                        title: title,
                    });
                }
                idcount[row.labels[0]]++;
                ids.add(row.id);
            }
        }
        // if edje
        else if (row.type === "relationship") {
            if (ids.has(row.start.id) && ids.has(row.end.id))
                edjes.push({
                    id: row.id,
                    from: row.start.id,
                    to: row.end.id,
                    label: row.label,
                });
        }
    });
    console.log(idcount);
    console.log(clusters);
    var graph = { nodes: nodes, edjes: edjes, clusters: clusters };
    console.log(count);
    return graph;
}
module.exports = parseGraph;
