const fs = require("fs");
const User = require("./../models/userModel");
const loginUrl = "login";
const formidable = require("formidable");
var welcomeHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/welcome.html`,
	"utf-8"
);
var loginHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/log-in.html`,
	"utf-8"
);
var signupHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/sign-up.html`,
	"utf-8"
);
var indexHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/indexx.html`,
	"utf-8"
);
exports.getAllUsers = (req, res) => {
	try {
		if (req.user) res.redirect("/welcome");
		else {
			res.writeHead(200, {
				"Content-type": "text/html",
				"my-own-header": "hello-world"
			});
			res.end(indexHTML);
		}
	} catch (err) {
		res.status(500).send(err);
	}
};

exports.createUser = async (req, res) => {
	try {
		const user = User(req.body);
		console.log(user);
		const result = await user.save();
		res.redirect(loginUrl);
	} catch (err) {
		console.log("err", err);
		res.status(500).send(err);
	}
};
exports.logout = (req, res) => {
	try {
		//console.log(req.user.email);
		req.session.destroy();
		req.user = null;
		res.redirect("/login");
	} catch (err) {
		res.status(500).send(err);
	}
};
exports.login = (req, res) => {
	try {
		if (req.user) res.redirect("/welcome");
		else {
			res.writeHead(200, {
				"Content-type": "text/html",
				"my-own-header": "hello-world"
			});
			res.end(loginHTML);
		}
	} catch (err) {
		res.status(500).send(err);
	}
};
exports.signup = (req, res) => {
	try {
		if (req.user) res.redirect("/welcome");
		else {
			res.writeHead(200, {
				"Content-type": "text/html",
				"my-own-header": "hello-world"
			});
			res.end(signupHTML);
		}
	} catch (err) {
		res.status(500).send(err);
	}
};
exports.welcome = async (req, res) => {
	try {
		//console.log(req.user.email);
		if (req.user) {
			res.writeHead(200, {
				"Content-type": "text/html",
				"my-own-header": "hello-world"
			});
			//console.log(welcomeHTML);
			let output = welcomeHTML.replace("{{USER_NAME}}", req.user.name);
			res.end(output);
		} else res.end(loginHTML);
	} catch (err) {
		res.status(500).send(err);
	}
};

exports.updateUser = (req, res) => {
	res.status(500).json({
		status: "error",
		message: "work in progress"
	});
};

exports.deleteUser = (req, res) => {
	res.status(500).json({
		status: "error",
		message: "work in progress"
	});
};

exports.uploadFile = (req, res) => {
	//disable upload
//	res.redirect("/mygraphs");

	let form = new formidable.IncomingForm();
	form.parse(req, function(err, fields, files) {
		console.log(files);
		let oldpath = files.malware.path;
		let fileName = req.user.id + files.malware.name;
		let newpath = "uploads/" + req.user.id + files.malware.name;
		if (files.malware.name ) {
			User.findOneAndUpdate(
				{ _id: req.user.id },
				{ $addToSet: { uploads: fileName } },
				function(error, success) {
					if (error) {
						console.log(error);
					} else {
						console.log(success);
					}
				}
			);
			fs.rename(oldpath, newpath, function(err) {
				if (err) throw err;

				res.redirect("/mygraphs");
			});
		} else res.redirect("/mygraphs");
	});
};
