# Malviz-Web

Malviz-web is a node.js application for visualizing malwares on a graph network.
It is a part of the [Malviz.ai](https://gitlab.com/shivamkataria2000/malviz.ai) application.

## Installation

Clone the repository and run the following commands.
Setup a mongo DB server and create a dotenv file with database connection details.

```bash
npm install
npm start
```

## Demo

[Malviz](http://malviz.herokuapp.com/)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
