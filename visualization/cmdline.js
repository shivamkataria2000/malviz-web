const fs = require("fs");

var report = fs.readFileSync(
	`${__dirname}/report.json`,
	"utf-8"
);
var reportObj=JSON.parse(report);
reportObj.signatures.forEach(function(row){
    //console.log(row.marks);
    row.marks.forEach((col)=>{
        if(col.category==="cmdline")
            console.log(col);
    });
});
