const fs = require("fs");
const Graph = require("./../models/graphModel");
const User = require("./../models/userModel");
const neo4jParser = require("./../visualization/neo4jParser");

var graphEgHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/graph-eg.html`,
	"utf-8"
);
var graphHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/graph.html`,
	"utf-8"
);
var cardsHTML = fs.readFileSync(
	`${__dirname}/../templates/templated-transit/cards-template.html`,
	"utf-8"
);
var uploadImgTag =
	'<div class="4u"><span class="image fit"><a href="/mygraph?file={{FILELINK}}"><img src="images/pic02.jpg" alt="" /></a><h2>{{FILENAME}}</h2></span></div>';
exports.getAllGraphs = async (req, res) => {
	try {
		//execute query
		const graphs = await Graph.find();

		// send respnse
		res.status(200).json({
			status: "success",
			requestedAt: req.requestTime, //
			results: graphs.length,
			data: {
				Graphs: graphs,
			},
		});
	} catch (err) {
		res.status(404).json({
			status: "fail",
			message: err,
		});
	}
};

exports.renderGraphTemplate = async (req, res) => {
	try {
		console.log("trying");
		const parsedGraph = neo4jParser();
		let nodes = JSON.stringify(parsedGraph.nodes);
		let edjes = JSON.stringify(parsedGraph.edjes);
		let clusterMap = parsedGraph.clusters.entries();
		let clusters = "[";
		for (let ele of clusterMap) {
			clusters += "[" + ele[1] + ',"' + ele[0] + '"]' + ",";
		}
		clusters = clusters.slice(0, -1);
		clusters += "]";
		console.log(clusters);
		let output = await graphEgHTML.replace("{{EDJES}}", edjes);
		output = await output.replace("{{NODES}}", nodes);
		output = await output.replace("{{CLUSTERS}}", clusters);
		console.log(output);
		res.writeHead(200, {
			"Content-type": "text/html",
		});
		res.end(output);
	} catch (err) {
		res.status(500).send(err);
	}
};

exports.getMyGraphs = async (req, res) => {
	try {
		let uploads = req.user.uploads;
		let uploadImgString = "";
		uploads.forEach((file) => {
			let fileName = file.slice(24);
			if (fileName.length > 24) fileName = fileName.slice(0, 24) + "..";
			let imgTag = uploadImgTag.replace(/{{FILENAME}}/g, fileName);
			imgTag = imgTag.replace(/{{FILELINK}}/g, file);
			uploadImgString += imgTag + "\n";
		});
		let output = await cardsHTML.replace("{{MYFILES}}", uploadImgString);
		output = output.replace("{{USER_NAME}}", req.user.name);
		res.writeHead(200, {
			"Content-type": "text/html",
		});
		res.end(output);
	} catch (err) {
		res.status(404).json({
			status: "fail",
			message: err,
		});
	}
};

exports.getGraph = async (req, res) => {
	try {
		let file = req.query.file;
		console.log(file);
		const uploads = req.user.uploads;
		if (uploads.find((fileName) => fileName === file)) {
			console.log("trying");
			file = file.split(".");
			file[file.length - 1] = "json";
			file = file.join(".");
			console.log(file);
			const parsedGraph = neo4jParser(
				`${__dirname}/../uploads/${file}`
			);
			let nodes = JSON.stringify(parsedGraph.nodes);
			let edjes = JSON.stringify(parsedGraph.edjes);
			let clusterMap = parsedGraph.clusters.entries();
			let clusters = "[";
			for (let ele of clusterMap) {
				clusters += "[" + ele[1] + ',"' + ele[0] + '"]' + ",";
			}
			clusters = clusters.slice(0, -1);
			clusters += "]";
			console.log(clusters);
			let output = await graphHTML.replace("{{EDJES}}", edjes);
			output = await output.replace("{{USER_NAME}}", req.user.name);
			output = await output.replace("{{NODES}}", nodes);
			output = await output.replace("{{CLUSTERS}}", clusters);
			console.log(output);
			res.writeHead(200, {
				"Content-type": "text/html",
			});
			res.end(output);
		} else {
			res.status(404).json({
				status: "fail",
				message: err,
			});
		}
	} catch (err) {
		res.status(404).json({
			status: "fail",
			message: err,
		});
	}
};

exports.createGraph = async (req, res) => {
	try {
		const newGraph = await Graph.create(req.body);

		res.status(201).json({
			status: "sucess",
			data: {
				graph: newGraph,
			},
		});
	} catch (err) {
		res.status(400).json({
			status: "Fail",
			message: err,
		});
	}
};
exports.updateGraph = async (req, res) => {
	try {
		const graph = await Graph.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
			runValidators: true,
		});
		console.log(graph);
		res.status(200).json({
			status: "success",
			data: {
				graph,
			},
		});
	} catch (err) {
		res.status(400).json({
			status: "fail",
			message: err,
		});
	}
};
exports.deleteGraph = async (req, res) => {
	try {
		const graph = await Graph.findByIdAndDelete(req.params.id);
		res.status(204).json({
			status: "success",
			data: null,
		});
	} catch (err) {
		res.status(400).json({
			status: "fail",
			message: err,
		});
	}
};

exports.getGraphStats = (req, res) => {
	res.status(501).json({
		status: "work in progress",
		message: err,
	});
};
