const fs = require("fs");
const http = require("http");
const url = require("url");
const querystring = require("querystring");
const formidable = require("formidable");
//const renderTemplate = require("./renderTemplate");
const malhtml = fs.readFileSync(`${__dirname}/templates/index.html`, "utf-8");
const file_upload = fs.readFileSync(
	`${__dirname}/templates/file-upload.html`,
	"utf-8"
);
const particlesApp = fs.readFileSync(
	`${__dirname}/templates/js/app.js`,
	"utf-8"
);
const particlesCss = fs.readFileSync(
	`${__dirname}/templates/css/style.css`,
	"utf-8"
);
const particlesJs = fs.readFileSync(
	`${__dirname}/node_modules/particles.js/particles.js`,
	"utf-8"
);

function processPost(request, response, callback) {
	let queryData = "";
	if (typeof callback !== "function") return null;

	if (request.method == "POST") {
		request.on("data", function(data) {
			queryData += data;
			if (queryData.length > 1e6) {
				queryData = "";
				response.writeHead(413, { "Content-Type": "text/plain" }).end();
				request.connection.destroy();
			}
		});

		request.on("end", function() {
			request.post = querystring.parse(queryData);
			callback();
		});
	} else {
		response.writeHead(405, { "Content-Type": "text/plain" });
		response.end();
	}
}

const server = http.createServer((req, res) => {
	const { query, pathname } = url.parse(req.url, true);
	if (pathname === "/") {
		res.writeHead(200, {
			"Content-type": "text/html"
		});
		res.end(malhtml);
	}
	if (pathname === "/node_modules/particles.js/particles.js") {
		res.writeHead(200, {
			"Content-type": "application/javascript"
		});
		res.end(particlesJs);
	}
	if (pathname === "/css/style.css") {
		res.writeHead(200, {
			"Content-type": "text/css"
		});
		res.end(particlesCss);
	}
	if (pathname === "/js/app.js") {
		res.writeHead(200, {
			"Content-type": "application/javascript"
		});
		res.end(particlesApp);
	}
	if (pathname === "/login") {
		if (req.method == "POST") {
			processPost(req, res, function() {
				console.log(req.post);
				// Use request.post here
				if (
					req.post["User[name]"] === "admin" &&
					req.post["User[password]"] === "malviz"
				) {
					res.writeHead(200, {
						"Content-type": "text/html"
					});
					res.end(file_upload);
				} else {
					res.writeHead(200, "OK", { "Content-Type": "text/plain" });
					res.end();
				}
			});
		}
	}
	if (pathname === "/login") {
		if (req.method == "POST") {
			processPost(req, res, function() {
				console.log(req.post);
				// Use request.post here
				if (
					req.post["User[name]"] === "admin" &&
					req.post["User[password]"] === "malviz"
				) {
					res.writeHead(200, {
						"Content-type": "text/html"
					});
					res.end(file_upload);
				} else {
					res.writeHead(200, "OK", { "Content-Type": "text/plain" });
					res.end();
				}
			});
		}
	}

	if (pathname == "/visualize") {
		let form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files) {
			console.log(files);
			let oldpath = files.malware.path;
			let newpath = "uploads/" + files.malware.name;
			fs.rename(oldpath, newpath, function(err) {
				if (err) throw err;
				res.write("File uploaded and moved!");
				res.end();
			});
		});
	}
	console.log(pathname);
});

server.listen(8000, "127.0.0.1", () => {
	console.log("Listening to requests on port 8000");
});
